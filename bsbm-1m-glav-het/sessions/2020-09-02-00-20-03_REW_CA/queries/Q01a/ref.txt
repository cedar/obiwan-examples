<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType8>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric6>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType8>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric5>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType8>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric4>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType56>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType57>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType56>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric1>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType56>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric2>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType57>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric1>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType8>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType56>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric3>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType57>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric2>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType56>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric4>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType57>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric3>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType56>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric5>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType57>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric4>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType56>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric6>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType57>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric5>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType57>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric6>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType8>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric3>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType8>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric2>,$value);

<$label,$featureLabel,$value> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType8>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,$featureLabel),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric1>,$value);

