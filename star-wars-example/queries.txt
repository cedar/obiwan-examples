# Find anyone who works for something that is a kind of organization
q<$x, $y> :-
      triple($x, <ex:uses>, $z),
      triple($z, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $y),
      triple($y, <http://www.w3.org/2000/01/rdf-schema#subClassOf>, <ex:Object>);

# Who uses light sabers, and how do they use vehicles
q1<$x, $y> :-
       triple($x, $y, $z),
       triple($x, <ex:uses>, $a),
       triple($a, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <ex:LightSaber>),
       triple($z, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $t),
       triple($t, <http://www.w3.org/2000/01/rdf-schema#subClassOf>, <ex:Vehicle>),
       triple($y, <http://www.w3.org/2000/01/rdf-schema#subPropertyOf>, <ex:uses>);
