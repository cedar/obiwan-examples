<$offer,$offerURL,$price,$deliveryDays> :- 
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>,$offerURL),
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/price>,$price),
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/deliveryDays>,$deliveryDays);