<$review,$product,$label,$personName,$title> :- 
	triple($product,<64561>,$label),
	triple($review,<461296>,$product),
	triple($review,<158620>,$person),
	triple($person,<33020>,$personName),
	triple($review,<59355>,$title);

