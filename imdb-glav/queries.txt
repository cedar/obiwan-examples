Q01<> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://dbpedia.org/ontology/Actor>),
	triple($x,<http://dbpedia.org/ontology/birthName>,"Aaker, Lee");

Q02<> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo");

Q03<> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#TVSeries>),
	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"24");

Q04<> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://dbpedia.org/ontology/Writer>),
	triple($x,<http://dbpedia.org/ontology/birthName>,"Barker, Clive");

Q05<> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Producer>),
	triple($x,<http://dbpedia.org/ontology/birthName>,"Silver, Joel");

Q06<> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://dbpedia.org/page/Film_Director>),
	triple($x,<http://dbpedia.org/ontology/birthName>,"Tarantino, Quentin");

Q06a<$y> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://dbpedia.org/page/Film_Director>),
	triple($x,<http://dbpedia.org/ontology/birthName>, $y);

# There is no mapping to rewrite this query:
Q06b<$y> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://dbpedia.org/page/Film_Director>),
        triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Producer>),
	triple($x,<http://dbpedia.org/ontology/birthName>, $y);

Q07<> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Editor>),
	triple($x,<http://dbpedia.org/ontology/birthName>,"Rawlings, Terry");

Q08<$y> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo"),
	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#belongsToGenre>,$y);

Q09<$y> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#TVSeries>),
	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"24"),
	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#belongsToGenre>,$y);

Q10<$y> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo"),
	triple($x,<http://dbpedia.org/ontology/budget>,$y);

Q10a<$y> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,$z),
	triple($x,<http://dbpedia.org/ontology/budget>,$y);

Q11<$y> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#TVSeries>),
	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"24"),
	triple($x,<http://dbpedia.org/ontology/budget>,$y);


Q01k<> :-
        triple($act, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://dbpedia.org/ontology/Actor>),
        triple($act,<http://dbpedia.org/ontology/birthDate>, "2000");

# Q12<$x,$y> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo"),
# 	triple($x,<http://dbpedia.org/ontology/gross>,$y);

# Q13<$x,$y> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#TVSeries>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"24"),
# 	triple($x,<http://dbpedia.org/ontology/gross>,$y);

# Q14<$x,$y> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo"),
# 	triple($x,<http://dbpedia.org/ontology/productionStartYear>,$y);

# Q15<$x,$y> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#TVSeries>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"24"),
# 	triple($x,<http://dbpedia.org/ontology/productionStartYear>,$y);

# Q16<$x,$z> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo"),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasActor>,$y),
# 	triple($y,<http://dbpedia.org/ontology/birthName>,$z);

# Q17<$x,$z> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo"),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasMaleActor>,$y),
# 	triple($y,<http://dbpedia.org/ontology/birthName>,$z);

# Q18<$x,$z> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo"),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasActress>,$y),
# 	triple($y,<http://dbpedia.org/ontology/birthName>,$z);

# Q19<$x,$z> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo"),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasDirector>,$y),
# 	triple($y,<http://dbpedia.org/ontology/birthName>,$z);

# Q20<$x,$z> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo"),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasProducer>,$y),
# 	triple($y,<http://dbpedia.org/ontology/birthName>,$z);

# Q21<$x,$z> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo"),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasEditor>,$y),
# 	triple($y,<http://dbpedia.org/ontology/birthName>,$z);

# Q22<$x,$z> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo"),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#isProducedBy>,$y),
# 	triple($y,<http://www.movieontology.org/2009/11/09/movieontology.owl#companyName>,$z);

# Q23<$title,$company> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,$title),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#isProducedBy>,$y),
# 	triple($y,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#East_Asian_Company>),
# 	triple($y,<http://www.movieontology.org/2009/11/09/movieontology.owl#companyName>,$company);

# Q24<$x,$title,$year,$rating> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,$title),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#imdbrating>,$rating),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#belongsToGenre>,$FV0),
# 	triple($FV0,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Brute_Action>),
# 	triple($x,<http://dbpedia.org/ontology/productionStartYear>,$year);

# Q25<$x,$title,$rating> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,$title),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#imdbrating>,$rating);

# Q26<$x,$genre,$production_year,$budget,$gross,$rating,$actor_name,$director_name,$producer_name,$editor_name> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo"),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#belongsToGenre>,$genre),
# 	triple($x,<http://dbpedia.org/ontology/productionStartYear>,$production_year),
# 	triple($x,<http://dbpedia.org/ontology/budget>,$budget),
# 	triple($x,<http://dbpedia.org/ontology/gross>,$gross),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#imdbrating>,$rating),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasActor>,$actor),
# 	triple($actor,<http://dbpedia.org/ontology/birthName>,$actor_name),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasDirector>,$director),
# 	triple($director,<http://dbpedia.org/ontology/birthName>,$director_name),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasProducer>,$producer),
# 	triple($producer,<http://dbpedia.org/ontology/birthName>,$producer_name),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasEditor>,$editor),
# 	triple($editor,<http://dbpedia.org/ontology/birthName>,$editor_name);

# Q27<$x,$company_name,$country_code> :- 
# 	triple($x,<http://www.movieontology.org/2009/11/09/movieontology.owl#hasCompanyLocation>,$y),
# 	triple($x,<http://www.movieontology.org/2009/11/09/movieontology.owl#companyName>,$company_name),
# 	triple($x,<http://www.movieontology.org/2009/11/09/movieontology.owl#countryCode>,$country_code),
# 	triple($y,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Western_Europe>);

# Q28<$y,$name,$movie_title,$prod_year> :- 
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,$movie_title),
# 	triple($x,<http://dbpedia.org/ontology/productionStartYear>,$prod_year),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#isProducedBy>,$y),
# 	triple($y,<http://www.movieontology.org/2009/11/09/movieontology.owl#companyName>,$name),
# 	triple($y,<http://www.movieontology.org/2009/11/09/movieontology.owl#hasCompanyLocation>,$FV0),
# 	triple($FV0,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Eastern_Asia>);

# Q29<$x,$title,$company_name> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,$title),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#isProducedBy>,$y),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#belongsToGenre>,$z),
# 	triple($y,<http://www.movieontology.org/2009/11/09/movieontology.owl#companyName>,$company_name),
# 	triple($y,<http://www.movieontology.org/2009/11/09/movieontology.owl#hasCompanyLocation>,$FV0),
# 	triple($FV0,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Eastern_Asia>),
# 	triple($z,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Brute_Action>);

# Q30<$x,$title,$actor_name,$company_name> :- 
# 	triple($m,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($m,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,$title),
# 	triple($m,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasActor>,$x),
# 	triple($m,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasDirector>,$x),
# 	triple($m,<http://www.movieontology.org/2009/10/01/movieontology.owl#isProducedBy>,$y),
# 	triple($m,<http://www.movieontology.org/2009/10/01/movieontology.owl#belongsToGenre>,$z),
# 	triple($x,<http://dbpedia.org/ontology/birthName>,$actor_name),
# 	triple($y,<http://www.movieontology.org/2009/11/09/movieontology.owl#companyName>,$company_name),
# 	triple($y,<http://www.movieontology.org/2009/11/09/movieontology.owl#hasCompanyLocation>,$FV0),
# 	triple($FV0,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Eastern_Asia>),
# 	triple($z,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Love>);

# Q31<$title> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://dbpedia.org/page/Film_Director>),
# 	triple($x,<http://dbpedia.org/ontology/birthName>,"Tarantino, Quentin"),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#isDirectorOf>,$y),
# 	triple($y,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,$title);

# Q32<$x,$title> :- 
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#belongsToGenre>,$y),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,$title),
# 	triple($y,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Actionreach>);

# Q33<$x,$name,$birthdate> :- 
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#isActorIn>,$y),
# 	triple($x,<http://dbpedia.org/ontology/birthName>,$name),
# 	triple($x,<http://dbpedia.org/ontology/birthDate>,$birthdate),
# 	triple($y,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo");

# Q34<$x,$budget,$gross,$director_name,$producer_name,$editor_name,$company_name> :- 
# 	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo"),
# 	triple($x,<http://dbpedia.org/ontology/budget>,$budget),
# 	triple($x,<http://dbpedia.org/ontology/gross>,$gross),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasDirector>,$director),
# 	triple($director,<http://dbpedia.org/ontology/birthName>,$director_name),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasDirector>,$producer),
# 	triple($producer,<http://dbpedia.org/ontology/birthName>,$producer_name),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasDirector>,$editor),
# 	triple($editor,<http://dbpedia.org/ontology/birthName>,$editor_name),
# 	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#isProducedBy>,$y),
# 	triple($y,<http://www.movieontology.org/2009/11/09/movieontology.owl#companyName>,$company_name);

# Q35<$x,$title,$actor_name,$prod_year,$rating> :- 
# 	triple($m,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
# 	triple($m,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,$title),
# 	triple($m,<http://www.movieontology.org/2009/10/01/movieontology.owl#imdbrating>,$rating),
# 	triple($m,<http://dbpedia.org/ontology/productionStartYear>,$prod_year),
# 	triple($m,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasActor>,$x),
# 	triple($m,<http://www.movieontology.org/2009/10/01/movieontology.owl#hasDirector>,$x),
# 	triple($x,<http://dbpedia.org/ontology/birthName>,$actor_name);


