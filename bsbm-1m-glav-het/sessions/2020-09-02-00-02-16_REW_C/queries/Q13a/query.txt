<$offer,$prop,$value> :- 
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>,$product),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType8>),
	triple($product,$prop,$value),
	triple($prop,<http://www.w3.org/2000/01/rdf-schema#domain>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/Product>);