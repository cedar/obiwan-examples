#+TITLE: Query Answering Test RIS

More details at: https://pages.saclay.inria.fr/maxime.buron/projects/qa-test/qa-test.html

** Loading instructions

Above, the instructions to load the datasets in postgres.
#+BEGIN_src shell
../../obiwan-tatooine-view/build.sh
#+END_src
