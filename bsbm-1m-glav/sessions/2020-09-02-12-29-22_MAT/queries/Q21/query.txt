<$product,$type> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,$type);