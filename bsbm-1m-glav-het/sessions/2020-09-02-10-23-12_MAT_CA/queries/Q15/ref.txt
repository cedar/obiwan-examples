<$review,$product,$label,$personName,$title> :- 
	triple($product,<69473>,$label),
	triple($review,<496303>,$product),
	triple($review,<170668>,$person),
	triple($person,<35541>,$personName),
	triple($review,<63844>,$title);

