<$offer,$offerURL> :- 
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/vendor>,$vendor),
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>,$offerURL),
	triple($vendor,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>,$country),
	triple($country,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType11>);

