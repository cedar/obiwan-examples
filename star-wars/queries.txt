# name of actor and spaceship whose character pilots it
Q01<$aname, $sname> :-
          triple($actor, <ex:performerOf>, $char),
          triple($actor, <http://www.w3.org/2000/01/rdf-schema#label>, $aname),
          triple($char, <ex:pilotOf> , $starship),
          triple($starship, <http://www.w3.org/2000/01/rdf-schema#label>, $sname);


# characters by starship they pilot
Q02<$char, $sname> :-
          triple($char, <ex:pilotOf> , $starship),
          triple($starship, <http://www.w3.org/2000/01/rdf-schema#label>, $sname);

# characters by transport starship they pilot
Q03<$char, $sname> :-
          triple($char, <ex:pilotOf> , $starship),
          triple($starship, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <ex:TransportStarShip>),
          triple($starship, <http://www.w3.org/2000/01/rdf-schema#label>, $sname);


# character name by actor name
Q04<$aName, $cName> :-
          triple($actor, <ex:performerOf> , $char),
          triple($actor, <http://www.w3.org/2000/01/rdf-schema#label>, $aName),
          triple($char, <http://www.w3.org/2000/01/rdf-schema#label>, $cName);

# list of character names
Q05<$cName> :-
          triple($char, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <ex:Character>),
          triple($char, <http://www.w3.org/2000/01/rdf-schema#label>, $cName);

# list of organisation names
Q06<$org> :-
          triple($org, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://dbpedia.org/ontology/Organisation>);

# member names by organisation
Q07<$cName, $org> :-
        triple($char, <http://www.w3.org/ns/org#memberOf>, $org),
        triple($char, <http://www.w3.org/2000/01/rdf-schema#label>, $cName);

# name of actor and spaceship whose character pilots it
Q08<$aname, $sname> :-
          triple($actor, <http://www.movieontology.org/2009/10/01/movieontology.owl#isActorIn>, $movie),
          triple($actor, <ex:performerOf>, $char),
          triple($actor, <http://www.w3.org/2000/01/rdf-schema#label>, $aname),
          triple($char, <ex:pilotOf> , $starship),
          triple($starship, <http://www.w3.org/2000/01/rdf-schema#label>, $sname);

# name of actor and spaceship whose character pilots it
Q09<$aname, $vehicleName, $vehicleClass> :-
          triple($actor, <http://www.movieontology.org/2009/10/01/movieontology.owl#isActorIn>, $movie),
          triple($actor, <ex:performerOf>, $char),
          triple($actor, <http://www.w3.org/2000/01/rdf-schema#label>, $aname),
          triple($char, <ex:pilotOf> , $vehicle),
          triple($vehicle, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $vehicleClass),
          triple($vehicleClass, <http://www.w3.org/2000/01/rdf-schema#subClassOf>, <ex:Vehicle>),
          triple($vehicle, <http://www.w3.org/2000/01/rdf-schema#label>, $vehicleName);

