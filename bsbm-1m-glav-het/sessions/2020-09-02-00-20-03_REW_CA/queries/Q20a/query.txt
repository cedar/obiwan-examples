<$offer,$offerURL,$price,$deliveryDays,$value> :- 
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>,$offerURL),
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/price>,$price),
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/deliveryDays>,$deliveryDays),
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>,$product),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType8>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>,$value),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>,$producer),
	triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>,$country),
	triple($country,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType12>);