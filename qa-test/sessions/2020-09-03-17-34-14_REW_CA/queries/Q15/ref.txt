<<Place>> :- 
	triple($FV_5,<livesIn>,<Maxime>);

<$object> :- 
	triple(<Maxime>,$prop,$object);

<<Thing>> :- 
	triple(<Maxime>,<phoneNumber>,$FV_6);

<<Person>> :- 
	triple(<Maxime>,<contact>,$FV_4);

<<Thing>> :- 
	triple($FV_7,<livesIn>,<Maxime>);

<<City>> :- 
	triple($FV_3,<livesIn>,<Maxime>);

<<Thing>> :- 
	triple(<Maxime>,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<Place>);

<<Person>> :- 
	triple(<Maxime>,<phoneNumber>,$FV_4);

<<Place>> :- 
	triple(<Maxime>,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<City>);

<<Thing>> :- 
	triple(<Maxime>,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<Person>);

<<Thing>> :- 
	triple(<Maxime>,<contact>,$FV_6);

<<Thing>> :- 
	triple(<Maxime>,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<City>);

