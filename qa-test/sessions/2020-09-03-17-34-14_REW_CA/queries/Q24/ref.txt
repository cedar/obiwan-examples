<<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,$city> :- 
	triple($FV_33,<livesIn>,<Ioana>),
	triple($city,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<City>);

<<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,$city> :- 
	triple(<Ioana>,<contact>,$FV_34),
	triple($city,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<City>);

<<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,$city> :- 
	triple($FV_33,<livesIn>,<Ioana>),
	triple($FV_45,<livesIn>,$city);

<<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,$city> :- 
	triple(<Ioana>,<phoneNumber>,$FV_34),
	triple($FV_47,<livesIn>,$city);

<$prop,$city> :- 
	triple(<Ioana>,$prop,$object),
	triple($FV_35,<livesIn>,$city);

<<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,$city> :- 
	triple(<Ioana>,<phoneNumber>,$FV_34),
	triple($city,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<City>);

<$prop,$city> :- 
	triple(<Ioana>,$prop,$object),
	triple($city,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<City>);

<<contact>,$city> :- 
	triple(<Ioana>,<phoneNumber>,$object),
	triple($FV_44,<livesIn>,$city);

<<contact>,$city> :- 
	triple(<Ioana>,<phoneNumber>,$object),
	triple($city,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<City>);

<<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,$city> :- 
	triple(<Ioana>,<contact>,$FV_34),
	triple($FV_46,<livesIn>,$city);

