<$review,$product,$rating> :- 
	triple($review,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/reviewFor>,$product),
	triple($review,<http://purl.org/stuff/rev#reviewer>,$reviewer),
	triple($review,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/rating>,$rating),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>,$producer),
	triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>,$country),
	triple($country,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType2>);