<$offer,$product,$label,$vendor,$price> :- 
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>,$product),
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/vendor>,$vendor),
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/price>,$price);