<$x,$y> :- 
	triple($x,<ex:uses>,$z),
	triple($z,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,$y),
	triple($y,<http://www.w3.org/2000/01/rdf-schema#subClassOf>,<ex:Object>);