<$product1,$product2> :- 
	triple($product1,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType12>),
	triple($product1,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>,"774"),
	triple($product1,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>,$producer),
	triple($product2,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType2>),
	triple($product2,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>,$producer);