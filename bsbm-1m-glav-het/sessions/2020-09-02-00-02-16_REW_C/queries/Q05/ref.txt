<$offer,$offerURL,$vendor,$vendorLabel,$vendorHomepage> :- 
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/vendor>,$vendor),
	triple($offer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>,$offerURL),
	triple($vendor,<http://www.w3.org/2000/01/rdf-schema#label>,$vendorLabel),
	triple($vendor,<http://xmlns.com/foaf/0.1/homepage>,$vendorHomepage);

