<$label,$value,$country> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType110>),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>,$value),
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>,$producer),
	triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>,$country),
	triple($country,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType1>);

