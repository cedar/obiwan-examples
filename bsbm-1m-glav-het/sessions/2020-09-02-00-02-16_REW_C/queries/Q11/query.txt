<$label,$p,$country> :- 
	triple($x,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($x,$p,$org),
	triple($org,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://xmlns.com/foaf/0.1/Organization>),
	triple($org,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>,$country),
	triple($country,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType222>);