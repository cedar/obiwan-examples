<$x,$class,$superProp,$value> :- 
	triple($x,$prop,$value),
	triple($prop,<http://www.w3.org/2000/01/rdf-schema#subPropertyOf>,$superProp),
	triple($x,$p,$agent),
	triple($p,<http://www.w3.org/2000/01/rdf-schema#range>,<http://xmlns.com/foaf/0.1/Agent>),
	triple($p,<http://www.w3.org/2000/01/rdf-schema#domain>,$class),
	triple($agent,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>,$country),
	triple($country,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType2>);