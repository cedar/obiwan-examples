<$x,<ex:StarShip>> :- 
	triple($x,<ex:uses>,$z),
	triple($z,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<ex:StarShip>);

<$x,<ex:Vehicle>> :- 
	triple($x,<ex:uses>,$z),
	triple($z,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<ex:Vehicle>);

<$x,<ex:LightSaber>> :- 
	triple($x,<ex:uses>,$z),
	triple($z,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<ex:LightSaber>);

