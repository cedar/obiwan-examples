
const transform = v => (v === 'N/A') ? 0 : v
Papa.parse("2019-05-22-00-00-00-MAT_CA/stats.csv", {
  download: true,
  header: true,
  skipEmptyLines: true,
  transform: transform,
  complete: function(MATCAResults) {

    Papa.parse("2019-05-22-00-00-00-MAT_SAT/stats.csv", {
      download: true,
      header: true,
      skipEmptyLines: true,
      transform: transform,
      complete: function(MATResults) {

        Papa.parse("2019-05-22-00-00-00-REW_C/stats.csv", {
          download: true,
          header: true,
          skipEmptyLines: true,
          complete: function(REWCResults) {

            Papa.parse("2019-05-22-00-00-00-REW_CA/stats.csv", {
              download: true,
              header: true,
              skipEmptyLines: true,
              complete: function(REWCAResults) {

                const data = {
                  "REW-CA": REWCAResults,
                  "REW-C": REWCResults,
                  "MAT": MATResults,
                  "MAT-CA": MATCAResults,
                }
                const strategies = Object.keys(data)

                // Assign the specification to a local variable vlSpec.
                var vlSpecNumberAns = {
                  $schema: 'https://vega.github.io/schema/vega-lite/v4.json',
                  data: {
                    values: data[strategies[0]].data
                  },
                  mark: 'bar',
                  encoding: {
                    x: {field: 'INPUT',
                        type: 'ordinal',
                        axis: {
                          title: 'query'
                        }
                       },
                    y: {
                      field: 'N_ANS',
                      type: 'quantitative',
                      aggregate: 'mean',
                      scale: {"type": "log", "base": 10, domainMin: 0.1},
                      axis: {
                        title: 'number of answers'
                      }
                    }
                  }
                };

                const timesData = []
                const temporalKeys = data[strategies[0]].meta.fields.filter(f => f[0] === 'T' && f !== 'T_TOTAL')
                const temporalKeysOrder = []

                for (let strategy of strategies) {
                  for(let entry of data[strategy].data) {
                    let index = 0
                    for (let key of temporalKeys) {
                      timesData.push({
                        strategy: strategy,
                        INPUT: entry.INPUT,
                        key: key,
                        keyOrder: index,
                        value: entry[key]
                      })
                      index++
                    }
                  }
                }

                var vlSpecTimes = {
                  $schema: 'https://vega.github.io/schema/vega-lite/v4.json',
                  data: {
                    values: timesData
                  },
                  mark: 'bar',
                  "selection": {
                    "brush": {"type": "multi", "fields": ["key"], "bind": "legend", "empty": "none"}
                  },
                  "transform": [
                    {"filter": { "not":{"selection": "brush"}}}
                  ],
                  encoding: {
                    "column": {
                      "field": "strategy",
                      axis: {
                        title: "strategies"
                      }
                    },
                    y: {
                      field: 'INPUT',
                      type: 'nominal',
                      axis: {
                        "title": "query"
                      }
                    },
                    x: {
                      aggregate: 'mean',
                      field: 'value',
                      type: "quantitative",
                      axis: {
                        title: "normalized duration"
                      },
                      "stack": "normalize"
                    },
                    color: {
                      field: 'key',
                      "scale": {
                        "domain": temporalKeys
                      },
                      type: 'nominal',
                      "axis": {
                        "title": "times"
                      }
                    },
                    order: {
                      field: "keyOrder", "type": "quantitative"
                    },
                    "tooltip": [
                      { "field": "value",
                        "type": "quantitative",
                        aggregate: "mean",
                        "title": "duration (ms)" },
                      { "field": "key", "type": "nominal", "title": "time" }
                    ]
                  }
                };

                const totalData = []

                for(let strategy of strategies) {
                  for (let entry of data[strategy].data) {
                    const obj = Object.assign({}, entry)
                    obj.strategy = strategy 
                    totalData.push(obj)
                  }
                }
                
                var vlSpecTotal = {
                  $schema: 'https://vega.github.io/schema/vega-lite/v4.json',
                  "config": {
                    "facet": {
                      "columns": 20
                    },
                  },
                  data: {
                    values: totalData
                  },
                  facet: {
                    field: 'INPUT',
                    type: 'ordinal',
                    axis: {
                      "title": "query"
                    }
                  },
                  spec: {
                    "width": { step: 15},
                    layer: [
                      {
                        mark: {
                          type: "bar",
                          "binSpacing": 0
                        },
                      },
                      {
                        mark: {
                          "type": "text",
                          "align": "center",
                          "baseline": "middle",
                          dy: -5
                        },
                        "encoding": {
                          "text": {"field": "N_REF", "type": "quantitative"}
                        },
                        color: {
                          value: "black"
                        }
                      }
                    ],
                    encoding: {
                      color: {
                        field: 'strategy',
                        type: 'nominal',
                        "axis": {
                          "title": "strategies"
                        }
                      },
                      x: {
                        field: 'strategy',
                        type: 'nominal',
                        "scale": {"padding": 0, step: 1},
                        "axis": null
                      },
                      y: {
                        field: 'T_TOTAL',
                        type: "quantitative",
                        aggregate: 'mean',
                        scale: {"type": "log", "base": 10, domainMin: 1},
                        axis: {
                          title: "time (ms)"
                        }
                      }
                    }
                  }
                };

                const rewData = []
                const rewKeys = ['N_REW', 'N_CLASH', 'N_COVER', 'N_CORE']
                const rewKeysOrder = []

                for (let strategy of strategies) {
                  for(let entry of data[strategy].data) {
                    const clash = isNaN(entry['N_CLASH']) ? 0 : entry['N_CLASH']
                    rewData.push({
                      strategy: strategy,
                      INPUT: entry.INPUT,
                      key: 'rest',
                      keyOrder: 0,
                      value: entry['N_COVER']
                    })
                    rewData.push({
                      strategy: strategy,
                      INPUT: entry.INPUT,
                      key: 'del_cover',
                      keyOrder: 1,
                      value: entry['N_REW'] - clash - entry['N_COVER']
                    })
                    rewData.push({
                      strategy: strategy,
                      INPUT: entry.INPUT,
                      key: 'del_clash',
                      keyOrder: 2,
                      value: clash
                    })
                  }
                }

                console.log(rewData)

                var vlSpecRew = {
                  $schema: 'https://vega.github.io/schema/vega-lite/v4.json',
                  data: {
                    values: rewData
                  },
                  facet: {
                    "column": {
                      "field": "strategy",
                      axis: {
                        title: "strategies"
                      }
                    }
                  },
                  spec: {
                    encoding: {
                      y: {
                        field: 'INPUT',
                        type: 'nominal',
                        axis: {
                          "title": "query"
                        }
                      },
                      x: {
                        aggregate: 'mean',
                        field: 'value',
                        type: "quantitative",
                        axis: {
                          title: "rewriting size"
                        },
                        stack: 'normalize'
                      },
                      color: {
                        field: 'key',
                        type: 'nominal',
                        "axis": {
                          "title": "rewriting optimization"
                        }
                      },
                      order: {
                        field: "keyOrder", "type": "quantitative"
                      },
                    },
                    layer: [
                      {
                        mark: 'bar',
                        encoding: {
                          "tooltip": [
                            { "field": "value",
                              "type": "quantitative",
                              aggregate: "mean",
                              "title": "size" },
                            { "field": "key", "type": "nominal", "title": "time" }
                          ]
                        }
                      },
                      {"mark": {"type": "text", align: "right", dx: -2},
                       "encoding": {
                         "text": {
                           "aggregate": "mean",
                           "field": "value",
                           "type": "quantitative"
                         },
                         color: {
                           "value": "white"
                         },
                         opacity: {
                           "condition": {"test": {field: 'value', aggregate: 'mean', equal: 0}, "value": 0},
                           "value": 1
                         }
                       }
                      }
                    ]
                  }
                };
                
                // Embed the visualization in the container with id `vis`
                vegaEmbed('#total-vis', vlSpecTotal);
                vegaEmbed('#times-vis', vlSpecTimes);
                vegaEmbed('#ans-vis', vlSpecNumberAns);
                vegaEmbed('#rew-vis', vlSpecRew);

              }
            });
          }
        });
      }
    });
  }
});

