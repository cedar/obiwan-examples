<$x,$y> :- 
	triple($x,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www.movieontology.org/2009/10/01/movieontology.owl#Movie>),
	triple($x,<http://www.movieontology.org/2009/10/01/movieontology.owl#title>,"Finding Nemo"),
	triple($x,<http://dbpedia.org/ontology/budget>,$y);

