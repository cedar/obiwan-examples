<$review,$product,$label,$personName,$title> :- 
	triple($product,<http://www.w3.org/2000/01/rdf-schema#label>,$label),
	triple($review,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/reviewFor>,$product),
	triple($review,<http://purl.org/stuff/rev#reviewer>,$person),
	triple($person,<http://xmlns.com/foaf/0.1/name>,$personName),
	triple($review,<http://purl.org/dc/elements/1.1/title>,$title);