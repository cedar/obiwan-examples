--
-- PostgreSQL database dump
--

-- Dumped from database version 10.13 (Ubuntu 10.13-1.pgdg18.04+1)
-- Dumped by pg_dump version 10.13 (Ubuntu 10.13-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: swcasting; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.swcasting (
    tconst character varying(100),
    ordering integer,
    nconst character varying(100),
    category character varying(100),
    job character varying(2000),
    "character" character varying(2000)
);


ALTER TABLE public.swcasting OWNER TO postgres;

--
-- Name: swgenre; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.swgenre (
    tconst character varying(100),
    genre character varying(100)
);


ALTER TABLE public.swgenre OWNER TO postgres;

--
-- Name: swperson; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.swperson (
    nconst character varying(100),
    primaryname character varying(1000),
    birthyear integer,
    deathyear integer
);


ALTER TABLE public.swperson OWNER TO postgres;

--
-- Name: swtitle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.swtitle (
    id integer,
    tconst character varying(100),
    titletype character varying(100),
    primarytitle character varying(2000),
    originaltitle character varying(2000),
    isadult integer,
    startyear integer,
    endyear integer,
    runtimeminutes integer
);


ALTER TABLE public.swtitle OWNER TO postgres;

--
-- Data for Name: swcasting; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.swcasting (tconst, ordering, nconst, category, job, "character") FROM stdin;
tt2488496	2	nm3915784	actor	\N	Finn
tt0120915	5	nm0000184	director	\N	\N
tt0080684	7	nm0001410	writer	screenplay by	\N
tt3748528	10	nm0000184	writer	based on characters created by	\N
tt2527336	8	nm0005086	producer	producer	\N
tt0080684	2	nm0000148	actor	\N	Han Solo
tt2488496	4	nm1727304	actor	\N	General Hux
tt2527338	2	nm0000434	actor	\N	Luke Skywalker
tt2488496	1	nm5397459	actress	\N	Rey
tt0086190	9	nm0564768	producer	producer	\N
tt0121765	6	nm0355054	writer	screenplay by	\N
tt0121766	5	nm0000184	director	\N	\N
tt2527336	6	nm0000184	writer	based on characters created by	\N
tt0086190	3	nm0000402	actress	\N	Princess Leia
tt0086190	5	nm0549658	director	\N	\N
tt2488496	7	nm1578335	writer	written by	\N
tt2527338	9	nm0000184	writer	based on characters created by	\N
tt0086190	8	nm0443599	producer	producer	\N
tt0076759	9	nm0852405	cinematographer	director of photography	\N
tt0121765	5	nm0000184	director	\N	\N
tt0120915	9	nm0123785	editor	\N	\N
tt0121765	1	nm0159789	actor	\N	Anakin Skywalker
tt0120915	1	nm0000191	actor	\N	Obi-Wan Kenobi
tt2488496	9	nm1333357	producer	producer	\N
tt0121766	4	nm0000168	actor	\N	Mace Windu
tt2527336	10	nm0006911	cinematographer	director of photography	\N
tt2527338	3	nm3485845	actor	\N	Kylo Ren
tt2527338	10	nm0005086	producer	producer	\N
tt0120915	3	nm0000204	actress	\N	Queen Amidala
tt0121766	10	nm0123785	editor	\N	\N
tt0076759	4	nm0000027	actor	\N	Ben Obi-Wan Kenobi
tt2527336	2	nm3915784	actor	\N	Finn
tt2527336	4	nm0000402	actress	\N	Leia Organa
tt3748528	4	nm0947447	actor	\N	Chirrut Îmwe
tt0080684	9	nm0476030	producer	producer	\N
tt2527338	8	nm1119880	writer	story by	\N
tt0086190	4	nm0001850	actor	\N	Lando Calrissian
tt2527338	7	nm2081046	writer	story by	\N
tt0121766	3	nm0000191	actor	\N	Obi-Wan Kenobi
tt0086190	10	nm0002354	composer	\N	\N
tt0121765	4	nm0000489	actor	\N	Darth Tyranus
tt2527336	5	nm0426059	director	\N	\N
tt3748528	7	nm0006904	writer	screenplay by	\N
tt0121766	1	nm0159789	actor	\N	Anakin Skywalker
tt0121766	9	nm0059242	editor	\N	\N
tt0080684	3	nm0000402	actress	\N	Princess Leia
tt2527338	1	nm0000402	archive_footage	\N	Leia Organa
tt2488496	5	nm0009190	director	\N	\N
tt0121765	9	nm0002354	composer	\N	\N
tt2488496	3	nm1209966	actor	\N	Poe Dameron
tt0120915	10	nm0809551	editor	\N	\N
tt0076759	6	nm0476030	producer	producer	\N
tt0080684	10	nm0564768	producer	producer	\N
tt0086190	7	nm0000184	writer	screenplay by	\N
tt2527336	9	nm0002354	composer	\N	\N
tt2488496	8	nm0000184	writer	based on characters created by	\N
tt0080684	5	nm0449984	director	\N	\N
tt0120915	7	nm0002354	composer	\N	\N
tt3748528	2	nm0526019	actor	\N	Cassian Andor
tt0120915	8	nm0005897	cinematographer	director of photography	\N
tt0086190	2	nm0000148	actor	\N	Han Solo
tt0121765	3	nm0000191	actor	\N	Obi-Wan Kenobi
tt3748528	8	nm0461306	writer	story by	\N
tt2527338	5	nm0009190	director	\N	\N
tt0076759	8	nm0002354	composer	\N	\N
tt0121766	7	nm0002354	composer	\N	\N
tt0121766	8	nm0005897	cinematographer	director of photography	\N
tt0076759	10	nm0156816	editor	film editor	\N
tt0076759	3	nm0000402	actress	\N	Princess Leia Organa
tt0121765	2	nm0000204	actress	\N	Padmé
tt0121765	7	nm0564768	producer	producer	\N
tt0080684	6	nm0102824	writer	screenplay by	\N
tt0121765	8	nm0650038	producer	producer	\N
tt2527336	3	nm0000434	actor	\N	Dobbu Scay
tt0086190	1	nm0000434	actor	\N	Luke Skywalker
tt0120915	4	nm0005157	actor	\N	Anakin Skywalker
tt0121765	10	nm0005897	cinematographer	director of photography	\N
tt2527336	3	nm0000434	actor	\N	Luke Skywalker
tt2527336	7	nm0074851	producer	producer	\N
tt2527338	4	nm5397459	actress	\N	Rey
tt2527338	6	nm0006516	writer	screenplay by	\N
tt0120915	2	nm0000553	actor	\N	Qui-Gon Jinn
tt0080684	4	nm0001850	actor	\N	Lando Calrissian
tt3748528	9	nm1729428	writer	story by	\N
tt0121765	4	nm0000489	actor	\N	Count Dooku
tt0076759	1	nm0000434	actor	\N	Luke Skywalker
tt0120915	3	nm0000204	actress	\N	Padmé
tt0121766	6	nm0564768	producer	producer	\N
tt0076759	7	nm0564768	producer	producer	\N
tt0086190	6	nm0001410	writer	screenplay by	\N
tt0076759	5	nm0000184	director	\N	\N
tt3748528	6	nm0919363	writer	screenplay by	\N
tt3748528	5	nm2284484	director	\N	\N
tt0121766	2	nm0000204	actress	\N	Padmé
tt2488496	10	nm0005086	producer	producer	\N
tt0076759	2	nm0000148	actor	\N	Han Solo
tt2527336	1	nm5397459	actress	\N	Rey
tt0080684	1	nm0000434	actor	\N	Luke Skywalker
tt2488496	6	nm0001410	writer	written by	\N
tt0080684	8	nm0000184	writer	story by	\N
tt3748528	3	nm0876138	actor	\N	K-2SO
tt3748528	1	nm0428065	actress	\N	Jyn Erso
tt0120915	6	nm0564768	producer	producer	\N
tt0086190	11	nm0000469	actor	\N	Darth Vader
tt0076759	11	nm0000469	actor	\N	Darth Vader
tt0080684	11	nm0000469	actor	\N	Darth Vader
tt3748528	11	nm1872855	actor	\N	Darth Vader
\.


--
-- Data for Name: swgenre; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.swgenre (tconst, genre) FROM stdin;
tt0076759	Action
tt0076759	Adventure
tt0076759	Fantasy
tt0080684	Action
tt0080684	Adventure
tt0080684	Fantasy
tt0086190	Action
tt0086190	Adventure
tt0086190	Fantasy
tt0120915	Action
tt0120915	Adventure
tt0120915	Fantasy
tt0121765	Action
tt0121765	Adventure
tt0121765	Fantasy
tt0121766	Action
tt0121766	Adventure
tt0121766	Fantasy
tt2488496	Action
tt2488496	Adventure
tt2488496	Sci-Fi
tt2527336	Action
tt2527336	Adventure
tt2527336	Fantasy
tt2527338	Action
tt2527338	Adventure
tt2527338	Fantasy
tt3748528	Action
tt3748528	Adventure
tt3748528	Sci-Fi
\.


--
-- Data for Name: swperson; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.swperson (nconst, primaryname, birthyear, deathyear) FROM stdin;
nm0000027	Alec Guinness	1914	2000
nm0000148	Harrison Ford	1942	\N
nm0000168	Samuel L. Jackson	1948	\N
nm0000184	George Lucas	1944	\N
nm0000191	Ewan McGregor	1971	\N
nm0000204	Natalie Portman	1981	\N
nm0000402	Carrie Fisher	1956	2016
nm0000434	Mark Hamill	1951	\N
nm0000469	James Earl Jones	1931	\N
nm0000489	Christopher Lee	1922	2015
nm0000553	Liam Neeson	1952	\N
nm0001410	Lawrence Kasdan	1949	\N
nm0001850	Billy Dee Williams	1937	\N
nm0002354	John Williams	1932	\N
nm0005086	Kathleen Kennedy	1953	\N
nm0005157	Jake Lloyd	1989	\N
nm0005897	David Tattersall	1960	\N
nm0006516	Chris Terrio	1976	\N
nm0006904	Tony Gilroy	1956	\N
nm0006911	Steve Yedlin	1975	\N
nm0009190	J.J. Abrams	1966	\N
nm0059242	Roger Barton	\N	\N
nm0074851	Ram Bergman	\N	\N
nm0102824	Leigh Brackett	1915	1978
nm0123785	Ben Burtt	1948	\N
nm0156816	Richard Chew	1940	\N
nm0159789	Hayden Christensen	1981	\N
nm0355054	Jonathan Hales	1937	\N
nm0426059	Rian Johnson	1973	\N
nm0428065	Felicity Jones	1983	\N
nm0443599	Howard G. Kazanjian	1942	\N
nm0449984	Irvin Kershner	1923	2010
nm0461306	John Knoll	1962	\N
nm0476030	Gary Kurtz	1940	2018
nm0526019	Diego Luna	1979	\N
nm0549658	Richard Marquand	1937	1987
nm0564768	Rick McCallum	1950	\N
nm0650038	Lorne Orleans	\N	\N
nm0809551	Paul Martin Smith	\N	\N
nm0852405	Gilbert Taylor	1914	2013
nm0876138	Alan Tudyk	1971	\N
nm0919363	Chris Weitz	1969	\N
nm0947447	Donnie Yen	1963	\N
nm1119880	Colin Trevorrow	1976	\N
nm1209966	Oscar Isaac	1979	\N
nm1333357	Bryan Burk	1968	\N
nm1578335	Michael Arndt	1965	\N
nm1727304	Domhnall Gleeson	1983	\N
nm1729428	Gary Whitta	\N	\N
nm1872855	Spencer Wilding	1972	\N
nm2081046	Derek Connolly	\N	\N
nm2284484	Gareth Edwards	1975	\N
nm3485845	Adam Driver	1983	\N
nm3915784	John Boyega	1992	\N
nm5397459	Daisy Ridley	1992	\N
\.


--
-- Data for Name: swtitle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.swtitle (id, tconst, titletype, primarytitle, originaltitle, isadult, startyear, endyear, runtimeminutes) FROM stdin;
76759	tt0076759	movie	Star Wars: Episode IV - A New Hope	Star Wars	0	1977	\N	121
80684	tt0080684	movie	Star Wars: Episode V - The Empire Strikes Back	Star Wars: Episode V - The Empire Strikes Back	0	1980	\N	124
86190	tt0086190	movie	Star Wars: Episode VI - Return of the Jedi	Star Wars: Episode VI - Return of the Jedi	0	1983	\N	131
120915	tt0120915	movie	Star Wars: Episode I - The Phantom Menace	Star Wars: Episode I - The Phantom Menace	0	1999	\N	136
121765	tt0121765	movie	Star Wars: Episode II - Attack of the Clones	Star Wars: Episode II - Attack of the Clones	0	2002	\N	142
121766	tt0121766	movie	Star Wars: Episode III - Revenge of the Sith	Star Wars: Episode III - Revenge of the Sith	0	2005	\N	140
2488496	tt2488496	movie	Star Wars: Episode VII - The Force Awakens	Star Wars: Episode VII - The Force Awakens	0	2015	\N	138
2527336	tt2527336	movie	Star Wars: Episode VIII - The Last Jedi	Star Wars: Episode VIII - The Last Jedi	0	2017	\N	152
2527338	tt2527338	movie	Star Wars: Episode IX - The Rise of Skywalker	Star Wars: Episode IX - The Rise of Skywalker	0	2019	\N	142
3748528	tt3748528	movie	Rogue One: A Star Wars Story	Rogue One	0	2016	\N	133
\.


--
-- PostgreSQL database dump complete
--

