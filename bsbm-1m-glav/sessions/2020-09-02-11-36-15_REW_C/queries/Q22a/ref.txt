<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType65>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType65>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType67>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType67>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType12>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType12>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType69>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType69>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType10>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType10>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType14>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType14>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType63>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType63>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType58>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType58>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType61>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType61>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType56>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType56>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType70>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType70>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType8>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType8>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType64>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType64>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType13>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType13>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType11>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType11>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType68>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType68>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType15>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType15>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType66>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType66>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType62>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType62>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType60>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType60>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType57>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType57>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType71>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType71>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType59>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType59>);

<$product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType9>> :- 
	triple($product,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>,$productFeature),
	triple($productFeature,<http://www.w3.org/2000/01/rdf-schema#label>,"biographies"),
	triple($product,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType9>);

